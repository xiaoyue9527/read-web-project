# 前端项目阅读

##  [vue-big-screen-plugin]( https://gitee.com/MTrun/vue-big-screen-plugin)

一个基于 Vue3、TypeScript、DataV、ECharts 框架的 " **数据大屏项目** "，使用 '.vue' 和 '.tsx' 文件实现界面，采用新版动态屏幕适配方案，支持数据动态刷新渲染、内部DataV、ECharts图表都支持自由替换。

由@[奔跑的面条](https://gitee.com/MTrun)开源

