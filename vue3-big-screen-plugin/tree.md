D:\vue\read-web\vue3-big-screen-plugin
├─.browserslistrc
├─.eslintrc.js
├─babel.config.js
├─LICENSE
├─package-lock.json
├─package.json
├─README.md
├─tree.md
├─tsconfig.json
├─src
|  ├─App.vue
|  ├─main.ts
|  ├─shims-plugins-d.ts
|  ├─shims-vue.d.ts
|  ├─views
|  |   ├─index
|  |   |   └index.vue
|  |   ├─centerRight2
|  |   |      └index.vue
|  |   ├─centerRight1
|  |   |      ├─index.vue
|  |   |      ├─chart
|  |   |      |   ├─draw.tsx
|  |   |      |   └index.tsx
|  |   ├─centerLeft2
|  |   |      ├─index.vue
|  |   |      ├─chart
|  |   |      |   ├─draw.tsx
|  |   |      |   └index.tsx
|  |   ├─centerLeft1
|  |   |      ├─index.vue
|  |   |      ├─chart
|  |   |      |   ├─draw.tsx
|  |   |      |   └index.tsx
|  |   ├─center
|  |   |   ├─index.vue
|  |   |   ├─chart
|  |   |   |   └draw.tsx
|  |   ├─bottomRight
|  |   |      ├─index.vue
|  |   |      ├─chart
|  |   |      |   ├─draw.tsx
|  |   |      |   └index.tsx
|  |   ├─bottomLeft
|  |   |     ├─index.vue
|  |   |     ├─chart
|  |   |     |   ├─draw.tsx
|  |   |     |   └index.tsx
|  ├─utils
|  |   ├─index.ts
|  |   └useDraw.ts
|  ├─store
|  |   └index.ts
|  ├─router
|  |   └index.ts
|  ├─constant
|  |    └index.ts
|  ├─components
|  |     ├─componentInstall.ts
|  |     ├─echart
|  |     |   └index.tsx
|  ├─common
|  |   ├─echart
|  |   |   ├─style
|  |   |   |   └theme.js
|  |   |   ├─map
|  |   |   |  └fujian.js
|  ├─assets
|  |   ├─logo.png
|  |   ├─pageBg.png
|  |   ├─scss
|  |   |  ├─index.scss
|  |   |  ├─style.scss
|  |   |  └_variables.scss
|  |   ├─icon
|  |   |  ├─demo.css
|  |   |  ├─demo_index.html
|  |   |  ├─iconfont.css
|  |   |  ├─iconfont.js
|  |   |  ├─iconfont.json
|  |   |  ├─iconfont.ttf
|  |   |  ├─iconfont.woff
|  |   |  └iconfont.woff2
├─public
|   ├─favicon.ico
|   └index.html